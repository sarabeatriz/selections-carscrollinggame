#include "obstacle.h"

///
/// Default constructor. The properties of the obstacles are set as follows:
/// * x_cone: x coordinates where the obstacle will be placed
/// * y_cone: y coordinates where the obstacle will be placed
///
Obstacle::Obstacle(QWidget *parent) :
    QWidget(parent)
{
    x_obstacle=300;
    y_obstacle=250;
    name = ":/resources/cone.png" ;
}

///
/// Setter for the obstacle picture
///
void Obstacle::setObs(string arg){
    
    if(arg == "cone") name = ":/resources/cone.png";
    
    else if (arg == "hole") name = ":/resources/hoyo.png";
    
    else if(arg == "monster") name = ":/resources/monster.png";
    
    else if(arg == "zombie") name = ":/resources/zombie.png";
    
    else if(arg == "it") name = ":/resources/it.png";
    
    else if(arg == "spongebob") name = ":/resources/spongebob.png";
    
    else if(arg == "patrick") name = ":/resources/patrick.png";
    
}

///
/// Getter for the obstacle picture
///
string Obstacle::getObs(){
    return name;
}

///
/// Setter for the X coordinate
///
void Obstacle::setXObstacle(int arg){
    x_obstacle = arg;
}

///
/// Setter for the Y coordinate
///
void Obstacle::setYObstacle(int arg){
    y_obstacle = arg;
}

///
/// Getter for the X coordinate
///
int Obstacle::getXObstacle(){
    return x_obstacle;
}

///
/// Getter for the Y coordinate
///
int Obstacle::getYObstacle(){
    return y_obstacle;
}


