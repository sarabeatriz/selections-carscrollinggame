# Estructuras de selección - Juego de desplazamiento de carro

![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)



En casi todas las instancias en las cuales queremos resolver un problema, seleccionamos entre distintas opciones dependiendo de si se cumplen o no ciertas condiciones. Los programas de computadoras se construyen para resolver problemas y, por lo tanto, deben tener una estructura que permita tomar decisiones y seleccionar alternativas. En C++, las selecciones se estructuran utilizando `if`, `else`, `else if` o `switch`. Muchas veces el uso de estas estructuras también envuelve el uso de expresiones de relación y operadores lógicos. En la experiencia de laboratorio de hoy, practicarás el uso de algunas estructuras de selección completando el diseño de una aplicación de juego de colisiones de carros con obstáculos en una pista.


## Objetivos:

1. Utilizar expresiones relacionales y seleccionar operadores lógicos adecuados para la toma de decisiones.
2. Aplicar estructuras de selección.


## Pre-Lab:

Antes de llegar al laboratorio debes haber:

1. Repasado los siguientes conceptos:

    a. operadores lógicos

    b. if, else, else if, switch

2. Estudiado los conceptos e instrucciones para la sesión de laboratorio.

3. Tomado el quiz Pre-Lab, disponible en Moodle.

---

---


## Juego de desplazamiento de carro

Los juegos de colisiones consisten en manejar un objeto esquivando o provocando la posible colisión contra otros objetos. La colisión puede descontar puntos o detener el juego. También hay colisiones con algunos objetos que acumulan puntos. En el juego de esta experiencia de laboratorio, el objeto que se maneja es un carrito, los objetos que causan que el juego se detenga son pinos, agujeros; entre otros, y los objetos que suman puntos son las banderas.

Los controles del juego son bien sencillos: la tecla con la flecha hacia arriba desplaza el carro hacia arriba y la tecla con la flecha hacia abajo desplaza el carro hacia abajo. Al jugar, el juego da la sensación de que el carro está en movimiento hacia la derecha usando un método muy simple: desplazando el fondo con sus obstaculos y banderas hacia la izquierda mientras el carro permanece en el mismo lugar. El jugador puede desplazar el carro hacia arriba o hacia abajo para esquivar los obstáculos y para capturar las banderas. De haber una colisión con un obstáculo el juego se detiene. El jugador puede continuar, marcando el botón de `Retry`.

---

---

!INCLUDE "../../eip-diagnostic/car-scrolling/es/diag-car-scrolling-01.html"
<br>

!INCLUDE "../../eip-diagnostic/car-scrolling/es/diag-car-scrolling-02.html"
<br>

---

---


## Sesión de laboratorio:

En esta experiencia de laboratorio practicarás el uso de expresiones matemáticas y estructuras de selección para implementar la detección de colisiones del carrito contra los obstáculos y contra las banderas.  

Tu tarea es completar el diseño de la aplicación del juego. 


### Ejercicio 1 - Familiarizarte con las funciones pre-definidas. 

El primer paso en esta experiencia de laboratorio es familiarizarte con las funciones (métodos) pre-definidas en el código. Invocarás algunas de estas funciones en el código que completarás para detectar las colisiones.  

#### Instrucciones

1. Carga a `QtCreator` el proyecto `CarScrollingGame`. Hay dos maneras de hacer esto:

       * Utilizando la máquina virtual: Haz doble “click” en el archivo `CarScrollingGame.pro` que se encuentra  en el directorio `/home/eip/labs/selections-carscrollinggame` de la máquina virtual.
       * Descargando la carpeta del proyecto de `Bitbucket`: Utiliza un terminal y escribe el comando `git clone http:/bitbucket.org/eip-uprrp/selections-carscrollinggame` para descargar la carpeta `selections-carscrollinggame` de `Bitbucket`. En esa carpeta, haz doble “click” en el archivo `CarScrollingGame.pro`.

2. Configura el proyecto. El proyecto consiste de varios archivos. **Solo escribirás código en el archivo** `work.cpp`**. No debes cambiar nada en los demás archivos.** 

3. Vas a necesitar algunos de los métodos definidos en los siguientes archivos para crear tu código.

       * `car.h` y `car.cpp`: contienen la definición de la clase `Car`, los métodos de esta clase y sus declaraciones.
       *  `flag.h` y `flag.cpp`: contienen la definición de la clase `Flag`, los métodos de esta clase y sus declaraciones.
       *  `obtstacle.h` y `obstacle.cpp`: contienen la definición de la clase `Obstacle`, los métodos de esta clase y sus declaraciones.
       * `play.h` y `play.cpp`: contienen la definición de la clase `Play`, los métodos de esta clase y sus declaraciones.

       Familiarízate con los métodos en estos archivos. Pon énfazis en los siguientes métodos:

       * `Car::getYCar()`: Devuelve la coordenada en $$Y$$ de la posición del carro en la pista.
       * `Flag::getXFlag()`: Devuelve la coordenada en $$X$$ de la posición de la bandera en la pista.
       * `Flag::getYFlag()`: Devuelve la coordenada en $$Y$$ de la posición de la bandera en la pista.
       * `Flag::hide()`: Esconde la bandera.  Desaparece de la pista.
       * `Obstacle::getXObstacle()`: Devuelve la coordenada en $$X$$ de la posición del obstáculo en la pista.
       * `Obstacle::getYObstacle()`: Devuelve la coordenada en $$Y$$ de la posición del obstáculo en la pista.
       * `Play::setScore(n)`: Recibe un número entero y lo suma a la puntuación del juego.
    
       Nota que no hay método `getXCar()` porque el carro no se desplaza en el eje de $$X$$.  

### Ejercicio 2 - Completar la función para cambiar la pista del juego.

En este ejercicio utilizarás la estructura de condición de C++ **switch** para cambiar los atributos de la pista. Completarás el método `setTrack` que se encuentra en el archivo `work.cpp`. Este método cambia el ambiente de la pista del juego dependiendo del valor que recibe el parámetro `track_type`.  

El método `setTrack` recibe un valor de tipo `Track` que puede ser:

   * `play::DAY` - para la pista de día
   * `play::NIGHT` - para la pista de la noche
   * `play::BEACH` - para la pista de la playa
   * `play::CANDYLAND` - para la pista de dulces

Los atributos de la pista que se pueden cambiar son: 

   * la imagen de la pista usando la función `setTrackPixmap()`
   * la imagen de los obstáculos usando la función `setObstaclesPixmap()`

La función `setTrackPixmap(Track)` ya está definida y recibe una variable de tipo `Track` que puede ser un valor entre: **play::DAY**, **play::NIGHT**, **play::BEACH**, **play::CANDYLAND**.

La función `setObstaclePixmap(string)` ya está definida y recibe una variable de tipo `string` que puede ser un valor entre: **"hole"**, **"cone"**, **"it"**, **"zombie"**, **"spongebob"**, **"patric"**, **"monster"**.

#### Instrucciones

Para completar la función `setTrack()`:

1. Cambia la imagen de la pista de acuerdo al valor `Track` recibido.

2. Cambia la imagen de los obstáculos utilizando la estructura de selección `switch` de modo que los obstáculos cambien de acuerdo al valor recibido por `setTrack()`. Si el tipo de pista que se recibe es:

       * `play::DAY` - los obstáculos sean de tipo "hole" o "cone" 
       * `play::NIGHT` - los obstáculos sean de tipo "it" o "zombie"
       * `play::BEACH` - los obstáculos sean de tipo "spongebob" o "patric"
       * `play::CANDYLAND` - los obstáculos sean de tipo "monster"

       En las opciones que tengan dos posibles obstáculos utilza `rand() % 2` para escoger aleatoriamente entre un obstáculo u otro. 


### Ejercicio 3 - Completar la función para colisiones con obstáculos. 

En este ejercicio completarás el método `obstacleCollision` que se encuentra en el archivo `work.cpp`. La función recibe un objeto de clase `Obstacle` y otro objeto de clase `Car` y debe detectar si hay colisión o no entre el carro y el obstáculo. La función devuelve cierto si hay colisión entre el carro y un obstáculo, y falso si no hay colisión.

Para detectar la colisión la función debe solicitar las coordenadas del obstáculo y la coordenada en $$Y$$ del carro.  Recuerda que el carro no se desplaza en la coordenada $$X$$, esa coordenada es fija y está guardada en una variable constante llamada `CARX`.  La colisión ocurre si el obstáculo tiene la misma coordenada en $$X$$ y está a cierta distancia hacia arriba y abajo de la coordenada en $$Y$$ como muestra la Figura 1. El rango de distancia del centro del carro hacia arriba y abajo se encuentra guardada en la variable constante `OBSTACLERANGE`.

Si se detecta una colisión la función debe devolver `true` y si no, debe devolver `false`.

---

![ColisionC.png](images/ColisionC.png)

**Figura 1.** La imagen muestra las coordenadas $$(CARX,Y)$$ que debe tener el obstáculo para que ocurra una colisión.

---

### Ejercicio 4 - Completar la función para colisiones con banderas.

En este ejercicio completarás el método `flagCollision` que se encuentra en el archivo `work.cpp`. La función recibe un objeto de la clase `Obstacle` y otro objeto de la clase `Flag` y debe detectar si hay colisión o no entre el carro y la bandera. Esta función es bien similar a la función del Ejericio 3, excepto que esta función no devuelve un valor. Las acciones que ocurren cuando se detecta la colisión se van a tomar dentro de la función.

En este caso si se detecta una colisión, se debe aumentar la puntuación del juego 30 puntos utilizando la función `setScore`, y se debe esconder la bandera utilizando la función `flag.hide()` para crear la ilusión de que se recogió la bandera en la colisión.


---

---

## Entregas

Utiliza "Entrega" en Moodle para entregar el archivo `work.cpp` que contiene las invocaciones y cambios que hiciste al programa. Recuerda utilizar buenas prácticas de programación, incluye el nombre de los programadores y documenta tu programa.

---

---

## Referencias
[1] Dave Feinberg, http://nifty.stanford.edu/2011/feinberg-generic-scrolling-game/
