#ifndef RACER_H
#define RACER_H

#include <QWidget>
#include <string>

using namespace std;

///
/// Class that has the attributes of the racecar such as:
/// * Y Coordinate
/// * Name
/// * Setters and Getters 
///
class Car : public QWidget
{
    Q_OBJECT
public:
    explicit Car(QWidget *parent = 0);
    int y_car;
    string name;
    void setCar(string);
    string getCar();
    void setYCar(int);
    int getYCar();

signals:

public slots:

};

#endif // RACER_H
