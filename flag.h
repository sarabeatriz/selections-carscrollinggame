#ifndef CHECKPOINT_H
#define CHECKPOINT_H

#include <QWidget>
#include <string>

using namespace std;

///
/// Class that has the attributes of the flags, such as:
/// * X, y coordinates
/// * Name
/// * Setters and Getters
///
class Flag : public QWidget
{
    Q_OBJECT
public:
    explicit Flag(QWidget *parent = 0);
    int y_flag;
    int x_flag;
    string name;
    int getXFlag();
    int getYFlag();
    void setXFlag(int);
    void setYFlag(int);
    void setFlag(string);
    string getFlag();
    void hide() ;
    
};

#endif // CHECKPOINT_H
